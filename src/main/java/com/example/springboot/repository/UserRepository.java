package com.example.springboot.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import com.example.springboot.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

    /*
     * Get user list by user name. Please note the format should be
     * findBy<column_name>.
     */
    List<User> findByUsername(String username);

    /*
     * Get user list by user name and password. Please note the format should be
     * findBy<column_name_1>And<column_name_2>.
     */
    List<User> findByUsernameAndPassword(String username, String password);

    @Transactional
    void deleteByUsernameAndPassword(String username, String password);

    @Transactional
    void deleteByUsername(String username);

}