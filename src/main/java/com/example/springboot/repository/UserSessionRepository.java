package com.example.springboot.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import com.example.springboot.entity.UserSession;


public interface UserSessionRepository extends CrudRepository<UserSession, Long> {

    /*
     * Get user list by user name. Please note the format should be
     * findBy<column_name>.
     */
//    List<UserSession> findByUsername(String username);

    /*
     * Get user list by user name and password. Please note the format should be
     * findBy<column_name_1>And<column_name_2>.
     */
//    List<UserSession> findByUsernameAndPassword(String username, String password);
    List<UserSession> findByUserid(long userid);

    @Transactional
    void deleteByUserid(String username);

}