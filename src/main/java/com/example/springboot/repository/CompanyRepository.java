package com.example.springboot.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.example.springboot.entity.Company;

public interface CompanyRepository extends CrudRepository<Company, Long> {

    /*
     * Get user list by user name. Please note the format should be
     * findBy<column_name>.
     */
    List<Company> findByName(String name);

    @Transactional
    void deleteByName(String name);

}