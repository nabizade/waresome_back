package com.example.springboot.exception;

public class UserSessionNotFoundException extends Exception {
    private long book_id;
    public UserSessionNotFoundException(long user_session_id) {
        super(String.format("Book is not found with id : '%s'", user_session_id));
    }
}