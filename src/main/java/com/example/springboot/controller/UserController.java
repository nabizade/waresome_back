package com.example.springboot.controller;

import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.example.springboot.exception.UserSessionNotFoundException;
import javassist.NotFoundException;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.example.springboot.entity.User;
import com.example.springboot.repository.UserRepository;
import com.example.springboot.entity.UserSession;
import com.example.springboot.repository.UserSessionRepository;

import static com.example.springboot.controller.SecureTokenGenerator.nextToken;

@Controller
@RequestMapping(path = "/api/v1/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserSessionRepository userSessionRepository;


    @RequestMapping(path = "/sign-in", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject signIn(@RequestBody JSONObject json) throws UserSessionNotFoundException {

        String name = json.getAsString("name");
        String password = json.getAsString("pass");

        List<User> userList = (List<User>) userRepository.findByUsernameAndPassword(name, password);

        JSONObject jsonresponse = new JSONObject();

        if (userList != null) {
            for (User user : userList) {
                Long user_id = user.getId();
                //sessiyani axtaririq varsa update yoxdursa yenisini yaradiriq
                List<UserSession> sessionList = (List<UserSession>) userSessionRepository.findByUserid(user_id);
                String token = "";
                if (!sessionList.isEmpty())
                    token = updateUserSession(user_id);
                else
                    token = createUserSession(user_id);
                jsonresponse.appendField("message", "OK");
                jsonresponse.appendField("token", token);
            }
        }

        if (userList.isEmpty()) {
            jsonresponse.appendField("error", "incorrect_username_or_password");
        }

        return jsonresponse;
    }

    public String updateUserSession(Long user_id) {

        List<UserSession> userSession = (List<UserSession>) userSessionRepository.findByUserid(user_id);

        String token = nextToken();
        userSession.get(0).setToken(token);

        Date date = new Date();
        userSession.get(0).setExpires_at(date);

        UserSession updatedSession = userSessionRepository.save(userSession.get(0));

        return token;

    }

    public String createUserSession(Long user_id){


        UserSession userSession = new UserSession();
        userSession.setUserid(user_id);

        String token = nextToken();
        userSession.setToken(token);

        Date date = new Date(System.currentTimeMillis());
        userSession.setExpires_at(date);

        UserSession updatedSession = userSessionRepository.save(userSession);

        return token;
    }



}