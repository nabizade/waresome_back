package com.example.springboot.controller;

import java.util.List;

import com.example.springboot.repository.CompanyRepository;
import com.fasterxml.jackson.databind.util.JSONPObject;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectSerializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.example.springboot.entity.Company;
import com.example.springboot.repository.UserAccountRepository;

@Controller
@RequestMapping(path = "/company")
public class CompanyController {

    @Autowired
    CompanyRepository CompanyRepository;

    /*
     * Mapping url exmaple: http://localhost:8080/userAccount/findAll
     */

    @RequestMapping(path = "/findAll", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject findAllCompany() {


        List<Company> companyList = (List<Company>) CompanyRepository.findAll();

        JSONObject json = new JSONObject();
        if (companyList != null) {
            for (Company company : companyList) {
                json.appendField("name", company.getName());
            }
        }

        if (companyList.isEmpty()) {
            json.appendField("error", "no_company");
        }



        return json;
    }



}