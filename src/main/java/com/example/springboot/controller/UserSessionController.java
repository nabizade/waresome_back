package com.example.springboot.controller;

import com.example.springboot.entity.UserSession;
import com.example.springboot.exception.UserSessionNotFoundException;
import com.example.springboot.repository.UserSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.SecureRandom;
import java.sql.Date;

public class UserSessionController {

    @Autowired
    UserSessionRepository userSessionRepository;

    public UserSession updateUserSession(Long userSessionId, Long user_id) throws UserSessionNotFoundException {

        UserSession userSession = userSessionRepository.findById(userSessionId).orElseThrow(() -> new UserSessionNotFoundException(userSessionId));

        userSession.setUserid(user_id);

        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        String token = bytes.toString();
        userSession.setToken(token);

        Date date = new Date(System.currentTimeMillis());
        userSession.setExpires_at(date);

        UserSession updatedSession = userSessionRepository.save(userSession);

        return updatedSession;
    }

}
